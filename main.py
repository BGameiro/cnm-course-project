#!/usr/bin/env python

from scipy.stats import norm
import matplotlib.pyplot as plt
import argparse, numpy, math, scipy.signal
from itertools import groupby
from operator import itemgetter
from moepy import lowess
from peakutils import baseline as bl

class peak:
    def __init__(self,spectrum_dict):
        self.spectrum = spectrum_dict
        self.update_coordinates()
    
    def update_coordinates(self):
        self.x_values = list(self.spectrum.keys())
        self.y_values = list(self.spectrum.values())

    def gaussian_fit(self, x):
        self.statistics()

        return lambda x: self.area*numpy.exp(-1/2*((x-self.mean)/self.stdev)**2)
    
    def remove_baseline(self):
        m = (self.y_values[-1]-self.y_values[0])/(self.x_values[-1]-self.x_values[0])
        b = self.y_values[0]-m*self.x_values[0]

        for i in self.spectrum.keys():
            self.spectrum[i] -= m*i+b
        
        self.update_coordinates()
    
    def statistics(self):
        y_values = self.y_values
        x_values = self.x_values
        
        #data = numpy.concatenate([[i]*int(self.spectrum[i]) for i in self.x_values])

        self.mean = numpy.average(self.x_values, weights=self.y_values)
        self.stdev = math.sqrt(numpy.average((self.x_values-self.mean)**2, weights=self.y_values))
        self.FWHM = 2*math.sqrt(2*math.log(10, math.e))*self.stdev
        self.area = numpy.trapz(self.y_values, self.x_values, dx=1)
    
    def get_mean(self):
        return self.mean
    
    def get_stdev(self):
        return self.stdev
    
    def get_FWHM(self):
        return self.FWHM
    
    def get_area(self):
        return self.area

def load_spectrum(spectrum_file):

    data = numpy.loadtxt(spectrum_file, dtype=float)

    if len(data.shape) == 1: # If only one column it doesn't the '1'
        spectrum = {k: v for k, v in enumerate(data)}
    else: # Only considers 2 first columns where first is data and second is channels/energy
        spectrum = {i[1]: i[0] for i in data}

    return spectrum

def background(spectrum_data, background_data, t=None):
    """
    >>> spectrum_dict = {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1}
    >>> background_dict = {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1}
    >>> background(spectrum_dict,background_dict,t=1)
    {0: 0.0, 1: 0.0, 2: 0.0, 3: 0.0, 4: 0.0, 5: 0.0, 6: 0.0}
    >>> background(spectrum_dict,background_dict,t=-1)
    Traceback (most recent call last):
    ...
    ValueError: The normalization contant needs to be positive.
    >>> background(spectrum_dict,background_dict,t="a")
    Traceback (most recent call last):
    ...
    TypeError: The normalization contant needs to be a float or an integer.
    >>> spectrum_dict = {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1}
    >>> background_dict = {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 7: 1} # Notice the 7 instead of the 6
    >>> background(spectrum_dict,background_dict,t=1)
    Traceback (most recent call last):
    ...
    ValueError: The spectrum and background need to have the same channels/energies.
    """

    if t:
        try:
            t = float(t)
        except:
            raise TypeError('The normalization contant needs to be a float or an integer.')
        if t <= 0:
            raise ValueError('The normalization contant needs to be positive.')

    while not t:
        user_t_input = input("Normalization contant [<float>/<int>/h]:")
        if user_t_input == "h":
            print("The normalization contant is how many times the background detection time is greater than the spectrum detection time.\
            \nFor example, for a background detection time of 10 minutes and a spectrum detection time of 2 minutes, the values is 5.")
        else:
            try:
                t = float(user_t_input)
            except:
                raise TypeError('The normalization contant needs to be a float or an integer.')
            if t <= 0:
                raise ValueError('The normalization contant needs to be positive.')

    if spectrum_data.keys() == background_data.keys():
        spectrum = {}
        for k in spectrum_data.keys():
            spectrum[k] = spectrum_data[k]-background_data[k]/float(t)
    else:
        raise ValueError('The spectrum and background need to have the same channels/energies.')

    return spectrum

def thresholding_algo(spectrum, threshold=4, lag=30, influence=0):
    """
    >>> signal = [1,2,1,2,1,2,1,3,1,2,1,1,1,1,1,1,1,2,5,6,9,8,5,1,1,1,1,1,1]
    >>> spectrum = {k:v for k,v in enumerate(signal)}
    >>> thresholding_algo(spectrum, threshold=4, lag=5, influence=0)
    {17: 2, 18: 5, 19: 6, 20: 9, 21: 8, 22: 5}
    >>> energies = [105., 115., 125., 135., 145., 155., 165., 175., 185., 195., 205.,215., 225., 235., 245., 255., 265., 275., 285., 295., 305., 315.,325., 335., 345., 355., 365., 375., 385.]
    >>> spectrum = {k:v for k,v in zip(energies,signal)}
    >>> thresholding_algo(spectrum, threshold=4, lag=5, influence=0)
    {275.0: 2, 285.0: 5, 295.0: 6, 305.0: 9, 315.0: 8, 325.0: 5}
    """
    x=list(spectrum.keys())
    y=list(spectrum.values())
    signals = numpy.zeros(len(y))
    filteredY = numpy.array(y)
    avgFilter = [0]*len(y)
    stdFilter = [0]*len(y)
    avgFilter[lag - 1] = numpy.mean(y[0:lag])
    stdFilter[lag - 1] = numpy.std(y[0:lag])
    peaks_dict = {}
    for i in range(lag, len(y)):
        if abs(y[i] - avgFilter[i-1]) > threshold * stdFilter [i-1]:
            if y[i] > avgFilter[i-1]:
                peaks_dict[x[i]] = y[i]
                signals[i] = 1
            else:
                signals[i] = 0 # Only want postive peaks

            filteredY[i] = influence * y[i] + (1 - influence) * filteredY[i-1]
            avgFilter[i] = numpy.mean(filteredY[(i-lag+1):i+1])
            stdFilter[i] = numpy.std(filteredY[(i-lag+1):i+1])
        else:
            signals[i] = 0
            filteredY[i] = y[i]
            avgFilter[i] = numpy.mean(filteredY[(i-lag+1):i+1])
            stdFilter[i] = numpy.std(filteredY[(i-lag+1):i+1])

    return peaks_dict

def peak_separation(peak_data):

    peak_list = []

    for i in [list(map(itemgetter(1), g)) for k, g in groupby(enumerate(peak_data), lambda x: x[0]-x[1])]:
        peak_list.append(
            peak(
                {k:peak_data[k] for k in i}
            )
        )

    return peak_list

def linear_regression(y,x):

    n=len(x)
    mean_x = numpy.mean(x)
    mean_y = numpy.mean(y)

    numerator = 0
    denominator = 0
    for i in range(n):
        numerator += (x[i] - mean_x) * (y[i] - mean_y)
        denominator += (y[i] - mean_x) ** 2
    slope=numerator/denominator
    c = mean_y - (slope * mean_x)

    return {"m":slope, "c":c}

def peak_detection(spectrum_data):
    """
    Returns a dictionary where the keys,values pairs are the x,y coordinates for the peaks.
    """

    user_input = None
    user_settings = {}

    while user_input != "y":
        if not user_settings:
            peak_data = thresholding_algo(spectrum_data)
        else:
            peak_data = thresholding_algo(spectrum_data, **user_settings)

        output_plot(spectrum_data,peak_data)

        user_input = input("Are the peaks correct?")
        if user_input != "y":
            user_settings["threshold"] = float(input("Threshold [default is 4]:"))
            user_settings["lag"] = int(input("Lag [default is 30]:"))
            user_settings["influence"] = float(input("Influence [default is 0]:"))

    return peak_data

def output_plot(spectrum_data,peak_data={}):
    """
    Outputs plots from spectrum data.
    """

    plt.clf() # Just to be safe

    plt.scatter(spectrum_data.keys(), spectrum_data.values(), marker="x", color='b')

    if peak_data:
        plt.scatter(peak_data.keys(), peak_data.values(), marker="x", color='r')
    
    plt.show()

def baseline_lowess(spectrum_data):

    y =  numpy.fromiter(spectrum_data.values(), dtype=float)
    x =  numpy.fromiter(spectrum_data.keys(), dtype=float)

    lowess_model = lowess.Lowess()
    lowess_model.fit(x,y)

    y_pred = lowess_model.predict(x)

    no_baseline = numpy.subtract(y, y_pred)
    output_plot(
        {k:v for k,v in zip(spectrum_data.keys(),no_baseline)}
    )

def baseline_peakutils(spectrum_data):

    user_input = None
    user_settings = {}

    while user_input != "y":
        if not user_settings:
            y =  numpy.fromiter(spectrum_data.values(), dtype=float)
            baseline_values = bl(y,deg=9,max_it=1000)
        else:
            baseline_values = bl(y,**user_settings)

        no_baseline = numpy.subtract(y, baseline_values)
        output_plot(
            {k:v for k,v in zip(spectrum_data.keys(),no_baseline)}
        )

        user_input = input("Is the baseline correct?")
        if user_input != "y":
            user_settings["deg"] = int(input("Degrees of the function [default is 9]:"))
            user_settings["max_it"] = int(input("Maximum iterations [default is 1000]:"))

def baseline_just_peaks(spectrum_data):

    peak_data = peak_detection(spectrum_data)
    peak_list = peak_separation(peak_data)

    spectrums = {}

    for p in peak_list:

        p.remove_baseline()
        #p.statistics()

        spectrums.update(p.spectrum)
    
    output_plot(spectrums)


def baseline(spectrum_data):
    """Provides a menu to choose between 3 different methods for baseline removal.
       A forth method based on user input coordinates was tested in the notebooks.
       Methods in this program: Lowess, Peakutils module, eliminate everything but the peaks.

    Args:
        spectrum_data: Dictionary where the keys are the channels or the energy and the values are the respective counts.
    """

    user_input = int(input("Select a baseline method [1/2/3]:\n\t[1] Lowess\n\t[2] Peakutils\n\t[3] Just peaks\n"))
    
    if user_input == 1:
        baseline_lowess(spectrum_data)
    elif user_input == 2:
        baseline_peakutils(spectrum_data)
    elif user_input == 3:
        baseline_just_peaks(spectrum_data)
    else:
        raise ValueError("Option not supported.")

def energy(spectrum_data):
    """Gets the spectrum data, calls a function to detect the peaks,
       separates the peaks by their x values in different peak objects,
       then, for each peak, removes the baseline, analyses the peak and
       asks the user for the energy values in order to make a linear regression,
       which is printed.

    Args:
        spectrum_data: Dictionary where the keys are the channels or the energy and the values are the respective counts.
    """

    peak_data = peak_detection(spectrum_data)
    peak_list = peak_separation(peak_data)

    peak_energies = {}
    i = 1

    for p in peak_list:

        p.remove_baseline()
        p.statistics()

        peak_energies[p.get_mean()] = float(input("What's the energy of peak #{}?".format(i)))
        i += 1
    
    lin_fit = linear_regression(list(peak_energies.keys()), list(peak_energies.values()))

    print("The relation between energy and channels is E={}*channel+{}".format(lin_fit["m"],lin_fit["c"]))

def peaks(spectrum_data):
    """Gets the spectrum data, calls a function to detect the peaks,
       separates the peaks by their x values in different peak objects,
       then, for each peak, removes the baseline, analyses the peak and
       prints the information.

    Args:
        spectrum_data: Dictionary where the keys are the channels or the energy and the values are the respective counts.
    """

    peak_data = peak_detection(spectrum_data)
    peak_list = peak_separation(peak_data)

    i=1
    for p in peak_list:

        p.remove_baseline() # Removing the base of the peak has a great effect on the area
        p.statistics()

        print(
            "Peak #{}:\n\tCentroid:\t{}\n\tFWHM:\t\t{}\n\tStdev:\t\t{}\n\tArea:\t\t{}".format(
                i,
                p.get_mean(),
                p.get_FWHM(),
                p.get_stdev(),
                p.get_area()
            )
        )

        i+=1

def arguments():
    """Gets the arguments.

    Returns:
        dict: Dictionary with the flags and the correspondent value passed or, in case none is passed, the default.
    """

    parser = argparse.ArgumentParser(allow_abbrev=False)

    parser.add_argument('command',
                        choices=['baseline', 'energy', 'peaks'],
                        type=str)
    
    parser.add_argument('spectrum_file',
                        type=str)
    
    parser.add_argument('--background_file',
                        '-b',
                        nargs='?',
                        const='./data/background.dat',
                        type=str)
    
    parser.add_argument('--background_times',
                        '-t',
                        nargs='?',
                        const=False,
                        type=float)

    #parser.add_argument('-o',
    #                    type=str,
    #                    nargs='?',
    #                    const='output.dat')

    return vars(parser.parse_args())

def output_info(command,spectrum_file,background_file):
    """Prints file information so that the user knows which files are being used and what command was called.

    Args:
        command: The command that's going to be applied to the data: 'baseline', 'energy', 'peaks'.
        spectrum_file: Path to file with spectrum data.
        background_file: Path to file with background data.
    """

    CRED = '\033[91m'
    CGRN = '\033[92m'
    CEND = '\033[0m'

    print("{}Command:{}\t{}\n{}Data:{}\t\t{}".format(CGRN,CEND,command,CGRN,CEND,spectrum_file))

    if background_file: 
        print("{}Background:{}\t{}".format(CGRN,CEND,background_file))


def menu(arguments_dict):
    """Receives the arguments from the terminal and runs one of the 3 available functions: 'baseline', 'energy', 'peaks'.

    Args:
        arguments_dict (dict): Dictionary with the flags and the correspondent value passed or, in case none is passed, the default. 
    """

    command = arguments_dict.pop("command")

    if command in ['baseline', 'energy', 'peaks']:

        spectrum_data = load_spectrum(arguments_dict["spectrum_file"])

        output_info(command,arguments_dict["spectrum_file"],arguments_dict["background_file"])

        if arguments_dict["background_file"]:
            background_data = load_spectrum(arguments_dict["background_file"])
            spectrum = background(spectrum_data, background_data, t=arguments_dict["background_times"])
        else:
            spectrum = spectrum_data

        output_data = globals()[command](spectrum)

if __name__ == "__main__":

    import doctest
    doctest.testmod()

    menu(arguments())