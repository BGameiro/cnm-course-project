# CNM Course Project

## Description

## Group members

- [Bernardo Gameiro ](mailto:contact@bgameiro.me)
- [Sai Kumar Chinthakayala](mailto:saikumar3005@gmail.com)

## Sources

- Streamlining Digital Signal Processing: A Tricks of the Trade Guidebook: Second Edition
- Retrieved January 22, 2022, from https://www.researchgate.net/publication/235660121_A_Simple_Algorithm_for_Fitting_a_Gaussian_Function
- Guo, H. (2012). A Simple Algorithm for Fitting a Gaussian Function

---

- Robust peak detection algorithm using z-scores
- Retrieved January 22, 2022, from https://stackoverflow.com/questions/22583391/peak-signal-detection-in-realtime-timeseries-data/22640362#22640362 
- Brakel, J. P. G. van. (2020, November 8)

---

- Gamma Detector Data Files
- Retrieved January 22, 2022, from https://www.cpp.edu/~pbsiegel/nuclear.html 
- Peter Siegel
